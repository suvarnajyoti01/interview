<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 10; $i++)
        { 
        	 DB::table('countries')->insert([
	            'name' => str_random(10),
	            'iso2' => str_random(10),
	            'iso3' => str_random(10),
	           	'created_at' => date("Y-m-d H:i:s"),
	           	'updated_at' => date("Y-m-d H:i:s"),
	        ]);
        }
    }
}