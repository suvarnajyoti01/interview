<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CountriesController extends Controller
{
    public function show()
    {

    	$countries = DB::select('select * from countries');
    	$json = json_encode($countries);
    	print_r($json);

		echo "<hr>";
        
        $data = Input::get();

        if(isset($data['iso2']))
        {
			$param = $data['iso2'];
			$result = DB::table('countries')->where('iso2', '=', $param)->get();

			if(count($result) == 0)
			{
				echo "No results found!";
			}
			else
			{
				echo "Record found !"."<br>"; 
				$res = json_encode($result);
				print_r($res);	
			}
        }
    }
}
